* 👋 Hello I'm Ari, a student and from Germany
* 🤓 I like to program in my freetime
* 📺 I watch a lot of anime and regualy hang out on discord
* 🖱️ Addicted to CS:GO and osu!
* 👷 Currently working on projects at my [organization](https://github.com/aridevelopment-de)
* 🗒️ Vim for life!

<hr />

**📊 [Weekly development breakdown](https://wakatime.com/@Ari24)**

